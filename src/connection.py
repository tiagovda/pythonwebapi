import psycopg2
import os
from design_patterns.singleton_meta import *


class Connection(metaclass=SingletonMeta):
    
    def get_db_connection(self):
        conn = psycopg2.connect(host=os.environ['POSTGRES_HOST'],
            database=os.environ['POSTGRES_DB'],
            user=os.environ['POSTGRES_USER'],
            password=os.environ['POSTGRES_PASSWORD'])
        return conn
