from flask import Flask, jsonify, request, Response
from psycopg2.extras import RealDictCursor
from sensor_def import *
from design_patterns.singleton_meta import *
from connection import *

app = Flask(__name__)


@app.route("/")
def hello():
    return "hello world!"

@app.route("/sensors", methods=["GET"])
def get_sensors():
    connection = Connection()
    conn = connection.get_db_connection()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute('SELECT * FROM sensors;')
    sensors = cur.fetchall()
    lista = []
    for sensor in sensors:
         a = Sensor(sensor['name'],sensor['description'],sensor['token'])
         lista.append(a.criptograr_token())
    cur.close()
    conn.close()
    return jsonify(sensors)

@app.route("/sensors/<id>", methods=["GET"])
def get_sensor_by_id(id):
    connection = Connection()
    conn = connection.get_db_connection()
    cur = conn.cursor(cursor_factory=RealDictCursor)
    try:
        cur.execute('SELECT * FROM sensors where id = %s;',id)
        sensors = cur.fetchone()
        sensor = Sensor(sensors['name'],sensors['description'],sensors['token'])
        return jsonify({
            'token': sensor.criptograr_token()
        })
        cur.close()
        conn.close()      
        if (sensors == None):
            return Response(status=404)
        return jsonify(sensors)
    except TypeError as e:
        return Response(status=500)

@app.route("/sensors", methods=["POST"])
def save_sensor():
    data = request.get_json()
    
    connection = Connection()
    conn = connection.get_db_connection()
    cur = conn.cursor()
    cur.execute('INSERT INTO sensors (name, description, token)'
            'VALUES (%(name)s, %(description)s, %(token)s) RETURNING ID',
            data
            )
    id = cur.fetchone()[0]
    conn.commit()
    cur.close()
    conn.close()
    data['id'] = id
    return jsonify(data)

@app.route("/sensors/<id>", methods=["PUT"])
def update_sensor(id):
    data = request.get_json()
    data['id'] = id
    connection = Connection()
    conn = connection.get_db_connection()
    cur = conn.cursor()
    cur.execute('UPDATE sensors SET name = %(name)s, description = %(description)s, token = %(token)s WHERE id = %(id)s',
            data
            )
    cur.close()
    conn.commit()
    conn.close()
    data['id'] = id
    return jsonify(data)

@app.route("/sensors/<id>", methods=["DELETE"])
def delete_sensor(id):
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute('DELETE FROM sensors WHERE id = %s',
            id
            )
    cur.close()
    conn.commit()
    conn.close()
    return Response(status=200)

if __name__ == "__main__":
    app.run(debug=True,host="0.0.0.0")

